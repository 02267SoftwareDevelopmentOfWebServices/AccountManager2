 	Feature: RegisterMerchant internal 
 		
 		Scenario: register as merchant
 		Given that my initial balance is 10
 		And that I am "not registered"    
 		When I register as "merchant" 
 		Then I am registered as "merchant" 
 		And I have a bank account
 		And I delete my bank account 
 		
 		Scenario: register as customer
 		Given that I am "not registered"    
 		Given that my initial balance is 100
 		When I register as "customer" 
 		Then I am registered as "customer" 
 		And I have a bank account
 		And I delete my bank account
 		
 		Scenario: register as customer - already registered
 		Given that I am "registered"    
 		Given that my initial balance is 100
 		When I register as "customer" 
 		Then I am registered as "customer" 
 		And I have a bank account
 		And I delete my bank account
 		
 		
 
 		