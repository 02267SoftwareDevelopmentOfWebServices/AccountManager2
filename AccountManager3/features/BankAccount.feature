	Feature: Bank account 
 		
 		Scenario: Get account by cpr without account
 		Given that my initial balance is 50
 		And that I am "registered as customer"    
 		When I get my account by CPR  
 		Then I have a bank account  
 		And I delete my bank account
 		
 		Scenario: Get account with balance of 0
 		Given that my initial balance is 0
 		And that I am "registered as customer"    
 		When I get my account by CPR  
 		Then I have a bank account  
 		And I delete my bank account
 		
 		Scenario: Get account by cpr without account
 		Given that I am "not registered"    
 		When I get my account by CPR  
 		Then I do not have a bank account  
 		And I delete my bank account
		
		Scenario: change first name account
 		Given that my initial balance is 50
 		And that I am "registered as customer"    
 		When I change my "first" name to "Kasper"  
 		Then my "first" name in the database is "Kasper" 
 		And I delete my bank account
 		
 		Scenario: change first name account
 		Given that my initial balance is 20
 		And that I am "registered as customer"    
 		When I change my "last" name to "Hansen"  
 		Then my "last" name in the database is "Hansen" 
 		And I delete my bank account
 		


 	