package asd.AccountManager3.rest;
import java.math.BigDecimal;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
/**
 * 
 * @author Kevin
 *
 */
@Path("/accounts/{cpr}")
public class AccountCprResource {
	
	//public static RPCServerInterface rpcInterface = new RPCServer(AccountsResource.am);
	
	@DELETE
	@Consumes ("text/plain")
	@Produces ("text/plain")
	/**
	 * This method deletes a bank account from the bank through REST
	 * @param cpr is the CPR number which is used to find the bank account
	 * @return a response, which is either OK if the method runs or a badrequest if the method fails.
	 * @throws Exception is thrown if the method is unable to run
	 */
	public Response deleteAccount(@PathParam("cpr") String cpr) throws Exception {

		CprNumber newCpr = new CprNumber(cpr); 
		try {
			System.out.println("deleting account");
			String accountId = AccountsResource.am.getAccountIdFromBank(newCpr);
			System.out.println("got ID from cpr: " + accountId);
			boolean result = AccountsResource.am.deleteCustomerAccount(accountId);
			//am.deleteMerchantAccount(accountId);
			System.out.println("account deleted");
			return Response.ok(""+result).build();
				
			
		} catch (Exception e) {
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}

	}
	@PUT
	@Consumes ("text/plain")
	@Produces ("text/plain")
	/**
	 * This method changes the first name of a user, through REST.
	 * @param cpr is the CPR number which is used to find the user.
	 * @param newFirstName is the new first name for the user.
	 * @return a response, which is the new first name if successful and null if not.
	 * @throws Exception is thrown if the method is unable to run.
	 */
	public Response changeFirstName(@PathParam("cpr") String cpr, String newFirstName) throws Exception {
		String result; 
		CprNumber newCpr = new CprNumber(cpr); 
		try {
			System.out.println("changing first name");
			 System.out.println("newCpr: " + newCpr.get());
			 System.out.println("new first name: " + newFirstName);
			User user = AccountsResource.am.changeFirstName(newCpr, newFirstName);
			if (user != null){
				result = user.getfirstName();
			}
			else{
				result = null;
			}
			System.out.println("first name changed");
			return Response.ok(result).build();
				
			
		} catch (Exception e) {
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}

	}
}
