package asd.AccountManager3.rest;

/**
 * 
 * @author Martin
 *
 */

public abstract class User {

	String firstName;
	String lastName;
	CprNumber cprNumber;

	/**
	 * User objects are used for both merchants and customers, since they use the same parameters.
	 * @param firstName is the first name of the user.
	 * @param lastName is the last name of the user.
	 * @param cprNumber is the CPR number, which is also used to identify the user.
	 */
	public User(String firstName, String lastName, CprNumber cprNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.cprNumber = cprNumber;

	}

	/**
	 * This method gets the users CPR number.
	 * @return the users CPR number as a CprNumber.
	 */
	public CprNumber getCpr() {
		return this.cprNumber;
	}
	/**
	 * This method gets the users first name.
	 * @return the users first name as a String
	 */
	public abstract String getfirstName();

	/**
	 * This method gets the users last name.
	 * @return the users last name as a String
	 */
	public abstract String getlastName();

	/**
	 * This method sets the users account id, which is used to identify the bank account.
	 * @param accountId is the id to identify the users bank account.
	 */
	public abstract void setAccountId(String accountId);
	
	/**
	 * This method gets the users account id
	 * @return The account id as a String
	 */
	public abstract String getAccountId();
	
	/**
	 * This method sets the users first name
	 * @param fName is the new first name for the user
	 */
	public abstract void setFirstName(String fName);
	
	/**
	 * This method sets the users last name
	 * @param lName is the new last name for the user
	 */
	public abstract void setLastName(String lName);

}
