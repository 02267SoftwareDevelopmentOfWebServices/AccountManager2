package asd.AccountManager3.rest;

/**
 * 
 * @author Anna
 *
 */
public interface CustomerRepository {

	public void addCustomer(Customer customer);
	public void removeCustomer(Customer customer);
	public boolean findCustomer(Customer customer);
}
