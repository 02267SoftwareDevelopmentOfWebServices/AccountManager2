package asd.AccountManager3.rest;

/**
 * 
 * @author Martin
 *
 */
public class CprNumber {

	private String value;

	/**
	 * CprNumber is a value object used to contain a CPR number.
	 * Having CPR number as a value object insures that it is always used correctly as a parameter
	 * and that the CPR number is correct to some extend 
	 */
	public CprNumber(String value) throws Exception {
		
		if (value.length()!=11) {
			
			throw new Exception(value);
		}
		
		if(value.contains("[a-zA-Z]+")){
			
			throw new Exception(value);
		}
		this.value = value;
	}
	
	/**
	 * This method gets the String the value object holds
	 * @return a String containing the CPR number 
	 */
	public String get()  {
		return this.value;
	}

}

