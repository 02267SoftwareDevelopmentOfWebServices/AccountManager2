package asd.AccountManager3.rest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Kevin
 *
 */

public class InMemoryRepository implements CustomerRepository {
	
	private List<Customer> customers = new ArrayList<Customer>();
	private List<Merchant> merchants = new ArrayList<Merchant>();
	
	// Check if customer ID already exists in DB.
	// Used when trying to register a new customer.
	
	/**
	 * this method checks if the customer exists in the repository
	 * @param id is the customers CPR number used to identify the customer
	 * @return a boolean, which is true if the customer exists and false otherwise.
	 */
	public boolean customerAlreadyExists(CprNumber id){
		for (int i =0; i<customers.size(); i++){
			if (customers.get(i).getCpr().get().equals(id.get())){
				System.out.println(customers.get(i).getCpr());
				return true;
			}
		}
		return false;
	}
	/**
	 * this method checks if the merchant exists in the repository
	 * @param id is the merchants CPR number used to identify the merchant
	 * @return a boolean, which is true if the merchant exists and false otherwise.
	 */
	public boolean merchantAlreadyExists(CprNumber id){
		for (int i =0; i<merchants.size(); i++){
			if (merchants.get(i).getCpr().get().equals(id.get())){
				System.out.println(merchants.get(i).getCpr());
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * This method is used to clear the repository, only used for testing purposes.
	 */
	public void clearRepository(){
		customers.clear();
		merchants.clear();
	}
	// Customer ----------------------------------
	/**
	 * this method adds a customer to the repository
	 * @param customer is the customer that is added to the repository
	 */
	public void addCustomer(Customer customer) {
		customers.add(customer);
	}
	
	/**
	 * this method removes a customer to the repository
	 * @param customer is the customer that is removed from the repository
	 */
	public void removeCustomer(Customer customer) {
		customers.remove(customer);
	}
	
	/**
	 * this method is used to check if the customer exists in the repository - not used
	 * @param customer is the customer object that is checked
	 * @return a boolean, which is true if the customer is in the repository. Returns false otherwise.
	 */
	public boolean findCustomer(Customer customer) {
		return customers.contains(customer);
	}
	/**
	 * this method is used to delete a customer
	 * @param accountId is the id used for the users bank account 
	 * @return a boolean, which is true if the customer deleted. Returns false otherwise.
	 */
	public boolean deleteCustomerFromAccountId(String accountId) {
		for (int i =0; i<customers.size(); i++){
			if (customers.get(i).getAccountId().equals(accountId)){
				customers.remove(i);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * this method is used to get a customer from the repository.
	 * @param customer is the customer object used to find the customer (The customers CPR number is used).
	 * @return a customer object, returns null if the customer isn't found.
	 */
	public Customer getCustomerFromCpr(Customer customer) {
		for (int i =0; i<customers.size(); i++){
			if (customers.get(i).getCpr().get().equals(customer.getCpr().get())){
				return customers.get(i);
			}
		}
		return null;
	}
	// Merchant ----------------------------------------
	/**
	 * this method adds a merchant to the repository
	 * @param merchant is the merchant that is added to the repository
	 */
	public void addMerchant(Merchant merchant) {
		merchants.add(merchant);
	}
	
	/**
	 * this method is used to check if the merchant exists in the repository - not used
	 * @param merchant is the merchant object that is checked
	 * @return a boolean, which is true if the merchant is in the repository. Returns false otherwise.
	 */
	public boolean findMerchant(Merchant merchant) {
		return merchants.contains(merchant);
	}
	
	/**
	 * this method is used to check if the merchant exists in the repository - not used
	 * @param merchant is the merchant object that is checked
	 * @return a boolean, which is true if the merchant is in the repository. Returns false otherwise.
	 */
	public boolean findMerchantAccountID(Merchant merchant) {
		return merchants.contains(merchant);
	}
	
	/**
	 * this method is used to delete a merchant
	 * @param accountId is the id used for the users bank account 
	 * @return a boolean, which is true if the merchant is deleted. Returns false otherwise.
	 */
	public boolean deleteMerchantFromAccountId(String accountId) {
		for (int i =0; i<merchants.size(); i++){
			System.out.println("deleteMerchantFromAccountId. Merchants.size = "+ merchants.size());
			if (merchants.get(i).getAccountId().equals(accountId)){
				merchants.remove(i);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * this method is used to get a merchant from the repository.
	 * @param merchant is the merchant object used to find the merchant (The merchants CPR number is used).
	 * @return a merchant object, returns null if the merchant isn't found.
	 */
	public Merchant getMerchantFromCpr(Merchant merchant) {
		for (int i =0; i<merchants.size(); i++){
			if (merchants.get(i).getCpr().equals(merchant.getCpr())){
				return merchants.get(i);
			}
		}
		return null;
	}
	



	/**
	 * This method is used to change a customer or merchant's first name.
	 * @param cpr is the CPR number of the user that is updated.
	 * @param newFirstName is the new first name, that is set.
	 * @return a User object, which is the user with the given CPR number. Returns null if the user isn't found
	 */
	public User changeFirstName(CprNumber cpr, String newFirstName) {
		System.out.println("change first name in repo");
		System.out.println("in memory rep customer size: " + customers.size());
		System.out.println("in memory rep merchants size: " + merchants.size());
		for (int i =0; i<customers.size(); i++){
			if (customers.get(i).getCpr().get().equals(cpr.get())){
				System.out.println("first: " +  customers.get(i).getfirstName());
				customers.get(i).setFirstName(newFirstName);
				System.out.println("second: " + customers.get(i).getfirstName());
				return customers.get(i);
			}
			if (merchants.get(i).getCpr().get().equals(cpr.get())){
				System.out.println("first: " +  merchants.get(i).getfirstName());
				merchants.get(i).setFirstName(newFirstName);
				System.out.println("second: " + merchants.get(i).getfirstName());
				return merchants.get(i);
			}
		}
		System.out.println("user not found");
		return null;
		
	}
	
	/**
	 * This method is used to change a customer or merchant's last name.
	 * @param cpr is the CPR number of the user that is updated.
	 * @param newLastName is the new first name, that is set.
	 * @return a User object, which is the user with the given CPR number. Returns null if the user isn't found
	 */
	public User changeLastName(CprNumber cpr, String newLastName) {
		for (int i =0; i<customers.size(); i++){
			if (customers.get(i).getCpr().get().equals(cpr.get())){
				customers.get(i).setLastName(newLastName);
				return customers.get(i);
			}
			if (merchants.get(i).getCpr().get().equals(cpr.get())){
				merchants.get(i).setLastName(newLastName);
				return merchants.get(i);
			}
		}
		return null;
		
	}
	
	/**
	 * This method is used to change a merchant's last name. - should not be used
	 * @param cpr is the CPR number of the user that is updated.
	 * @param newLastName is the new first name, that is set.
	 * @return a User object, which is the user with the given CPR number. Returns null if the user isn't found
	 */
	public Merchant changeLastNameMerchant(Merchant merchant, String newLastName) {
		for (int i =0; i<merchants.size(); i++){
			if (merchants.get(i).getCpr().equals(merchant.getCpr())){
				merchants.get(i).setLastName(newLastName);
				return merchants.get(i);
			}
		}
		return null;
	}
	
	



}
