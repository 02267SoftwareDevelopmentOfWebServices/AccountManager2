package demo.test;
import com.example.demo.rest.*;
import java.math.BigDecimal;
import static org.junit.Assert.*;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * 
 * @author Martin
 *
 */
public class ManageMerchantAndCustomerTest {

	BigDecimal initBalance;	
	Merchant merchant;
	Customer customer;
	String accountId;
	String type = ""; //TODO change so that it tests for specific non-registered users. 
	boolean accountExists;
	AccountManager accountManager; 
	@Before
	public void before() throws Exception{
		accountManager = new AccountManager();
		//remember to restart
		//make better cucumber test - if you are registered 
		merchant = new Merchant("Mr", "glass", new CprNumber("143847-8236"));
		customer = new Customer("Olna", "onguyen", new CprNumber("143343-2836"));
	}
	@Given("^that my initial balance is (\\d+)$")
	public void thatMyInitialBalanceIs(int balance) throws Throwable {
		initBalance = new BigDecimal(balance); 
		// Write code here that turns the phrase above into concrete actions
	
	}
	@Given("^that I am \"([^\"]*)\"$")
	public void thatIAm(String regStatus) throws Throwable {
		if(regStatus.equals("registered as customer")){
			//System.out.println(initBalance);
			accountManager.registerCustomerBank(customer, initBalance);
			accountManager.registerCustomer(customer);
			type = "customer";
		}
		else if (regStatus.equals("registered as merchant")){
			accountManager.registerMerchantBank(merchant, initBalance);
			accountManager.registerMerchant(merchant); 
			type = "merchant";
		}
		// Write code here that turns the phrase above into concrete actions

	}



	@When("^I register as \"([^\"]*)\"$")
	public void iRegisterAs(String userType) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		type = userType;
		if (userType.equals("customer")){
			accountManager.registerCustomerBank(customer, initBalance);
			accountManager.registerCustomer(customer);
			//System.out.println(AccountManager.registerCustomer(customer, initBalance));
		}
		else {
			accountManager.registerMerchantBank(merchant, initBalance);
			System.out.println("register as merchant TEST");
			accountManager.registerMerchant(merchant); 
		}

	}
	@When("^I get my account by CPR$")
	public void iGetMyAccountByCPR() throws Throwable {
		if (type.equals("customer")){
			accountManager.bankAccountExistsFromCpr(customer.getCpr().get());
		}
		else{
			accountManager.bankAccountExistsFromCpr(merchant.getCpr().get());
		}
		// Write code here that turns the phrase above into concrete actions
	}
//	@When("^I change my first name to \"([^\"]*)\"$")
//	public void iChangeMyFirstNameTo(String newFirstName) throws Throwable {
//	    // Write code here that turns the phrase above into concrete actions
//		if(type.equals("customer")){
//			AccountManager.changeFirstName(customer, newFirstName);
//		}
//	}
	
	@When("^I change my \"([^\"]*)\" name to \"([^\"]*)\"$")
	public void iChangeMyNameTo(String nameType, String newName) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		if(type.equals("customer")){
			if(nameType.equals("first")){
				accountManager.changeFirstName(customer.getCpr(), newName);
			}else{
				accountManager.changeLastName(customer.getCpr(), newName);
			}
		}else{
			if(nameType.equals("first")){
				accountManager.changeFirstName(merchant.getCpr(), newName);
			}else{
				accountManager.changeLastName(merchant.getCpr(), newName);
			}
		}
	}

	@Then("^my \"([^\"]*)\" name in the database is \"([^\"]*)\"$")
	public void myNameInTheDatabaseIs(String nameType, String newName) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		if(type.equals("customer")){
			if(nameType.equals("first")){
				assertEquals(newName, accountManager.getRepo().getCustomerFromCpr(customer).getfirstName());
			}else{
				assertEquals(newName, accountManager.getRepo().getCustomerFromCpr(customer).getlastName());
			}
		}else{
			if(nameType.equals("first")){
				assertEquals(newName, accountManager.getRepo().getMerchantFromCpr(merchant).getfirstName());
			}else{
				assertEquals(newName, accountManager.getRepo().getMerchantFromCpr(merchant).getlastName());
			}
		}
	}

	@Then("^my first name in the database is \"([^\"]*)\"$")
	public void myFirstNameInTheDatabaseIs(String newFirstName) throws Throwable {
		if(type.equals("customer")){
			assertEquals(newFirstName, accountManager.getRepo().getCustomerFromCpr(customer).getfirstName());
		}
		
		
	    // Write code here that turns the phrase above into concrete actions
	}

	@Then("^I have an account$")
	public void iHaveAnAccount() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		throw new PendingException();
	}

	@Then("^I am registered as \"([^\"]*)\"$")
	public void iAmRegisteredAs(String userType) throws Throwable {
		boolean registered;
		if (userType.equals("customer")){
			//registered = AccountManager.getRepo().findCustomer(customer);
			registered = accountManager.customerExists(customer.getCpr());
		}
		else{
			//registered = AccountManager.getRepo().findMerchant(merchant);
			registered = accountManager.merchantExists(merchant.getCpr());
		}
		assertTrue(registered);
		// Write code here that turns the phrase above into concrete actions
	}



	


	@Then("^I have a bank account$")
	public void iHaveABankAccount() throws Throwable {
		//assertNotNull(merchant.getAccountId());
		if(type.equals("customer")){
			//assertNotNull(BankServiceConnect.getAccount(customer.getAccountId()));
			assertTrue(accountManager.bankAccountExistsFromCpr(customer.getCpr().get()));
			//String accountID = BankServiceConnect.getAccountFromCpr(customer.getCpr().get()).getId();
			//BankServiceConnect.deleteAccount(accountID);
		}
		else if (type.equals("merchant")){
			assertTrue(accountManager.bankAccountExistsFromCpr(merchant.getCpr().get()));
			//assertNotNull(BankServiceConnect.getAccount(merchant.getAccountId()));

			//String accountID = BankServiceConnect.getAccountFromCpr(merchant.getCpr().get()).getId();
			//BankServiceConnect.deleteAccount(accountID);
		}

		//System.out.println(merchant.getAccountId());

		
	}
	@Then("^I do not have a bank account$")
	public void iDoNotHaveABankAccount() throws Throwable {
		if(type.equals("customer")){
			//assertNotNull(BankServiceConnect.getAccount(customer.getAccountId()));
			assertFalse(accountManager.bankAccountExistsFromCpr(customer.getCpr().get()));
			//String accountID = BankServiceConnect.getAccountFromCpr(customer.getCpr().get()).getId();
			//BankServiceConnect.deleteAccount(accountID);
		}
		else if (type.equals("merchant")){
			assertFalse(accountManager.bankAccountExistsFromCpr(merchant.getCpr().get()));
			//assertNotNull(BankServiceConnect.getAccount(merchant.getAccountId()));

			//String accountID = BankServiceConnect.getAccountFromCpr(merchant.getCpr().get()).getId();
			//BankServiceConnect.deleteAccount(accountID);
		}
	    
	}
	@Then("^I delete my bank account$")
	public void iDeleteMyBankAccount() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		if(type.equals("customer")){
			//assertNotNull(BankServiceConnect.getAccount(customer.getAccountId()));

			String accountID = accountManager.getAccountIdFromBank(customer.getCpr());
			accountManager.deleteCustomerAccount(accountID);
		}
		else if (type.equals("merchant")){
			//assertNotNull(BankServiceConnect.getAccount(merchant.getAccountId()));

			String accountID = accountManager.getAccountIdFromBank(merchant.getCpr());
			accountManager.deleteMerchantAccount(accountID);
		}
	}
	@After
	public void after() throws Exception{
		//BankServiceConnect.getAccountFromCpr(merchant.getCpr().get()).getId();
		//String accountID = BankServiceConnect.getAccountFromCpr(customer.getCpr().get()).getId();
		//BankServiceConnect.deleteAccount(accountID);
	}

}
