package com.example.demo.rest;

import java.math.BigDecimal;

import dtu.ws.fastmoney.*;

/**
 * 
 * @author Martin
 *
 */
public class BankServiceConnect {

	private BankServiceService service;
	private BankService port;

	public BankServiceConnect() {
		this.service = new BankServiceService();
		this.port =  service.getBankServicePort();
	}

	/**
	 * This method creates a new bank account for a user with a given initial balance  
	 * @param user is the customer or merchant object to be registered in the bank
	 * @param initBalance is the initial amount of money on the new bank account
	 * @return a string with the account id for the bank account, returns null if the user already has an account
	 */
	public String createAccountWithBalance(User user, BigDecimal initBalance) {
		String accountId = "";
		dtu.ws.fastmoney.User bankUser = new dtu.ws.fastmoney.User();
		bankUser.setCprNumber(user.getCpr().get());
		bankUser.setFirstName(user.getfirstName());
		bankUser.setLastName(user.getlastName());

		System.out.println("BankUser created");
		try {
			accountId = this.port.createAccountWithBalance(bankUser, initBalance);
			System.out.println("Bank Account created");
			return accountId;
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
			System.out.println("Bank account not created!!!");
		}
		return null;

	}
	
	/**
	 * This method gets the current balance for the bank account with the given account id  
	 * @param accountId is the id used to identify the account
	 * @return a bigDecimal with the account balance
	 */
	public BigDecimal getAccountBalance(String accountId) {

		try {
			Account account = this.port.getAccountByCprNumber(accountId);
			return account.getBalance();
		} catch (BankServiceException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	/**
	 * This method deletes a bank account
	 * @param accountId is the id used to identify the account
	 * @return a boolean which is true if the account was succesfully deleted and false otherwise
	 */
	public boolean deleteAccount(String accountId) {

		try {
			this.port.retireAccount(accountId);
			return true;
		} catch (BankServiceException_Exception e) {

			return false;
		}

	}

	/**
	 * This method gets a bank account from the server, by use of a cpr number
	 * @param cprNumber is the users CPR number, which is used to identify the user
	 * @return an account object. Returns null if the account isn't found
	 */
	public Account getAccountFromCpr(String cprNumber ){
		Account account;
		try {
			account = this.port.getAccountByCprNumber(cprNumber);
			return account;
		} catch (BankServiceException_Exception e) {
			return null; 
		}
	}

	/**
	 * This method gets a bank account from the server, by use of an account id
	 * @param accountId is the id used to identify the account
	 * @return an account object. Returns null if the account isn't found
	 */
	public Account getAccount(String accountId ){
		Account account;
		try {
			account = this.port.getAccount(accountId);
			return account;
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
