package com.example.demo.rest;

/**
 * 
 * @author Anna
 *
 */
public interface CustomerRepository {

	public void addCustomer(Customer customer);
	public void removeCustomer(Customer customer);
	public boolean findCustomer(Customer customer);
}
