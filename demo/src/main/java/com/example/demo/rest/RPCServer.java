package com.example.demo.rest;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.*;

/**
 * 
 * @author Kevin
 *
 */

public class RPCServer implements RPCServerInterface{

	private static final String RPC_QUEUE_NAME = "rpc_queue";
	private AccountManager am;

	/**
	 * The RPCServer is used to receive messages from the message queues
	 * @param accountManager is instance of accountManager used for in memory repository
	 */
	public RPCServer(AccountManager accountManager) {

		try {
			this.am = accountManager;
			System.out.println("Starting RPC server");

			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("172.18.0.1");
			System.out.println("IP is GOOOOOOOD");
			System.out.println("Host: " + factory.getHost() + " Port: " + factory.getPort());
			
			Connection connection = factory.newConnection();
			System.out.println("after connection");
			Channel channel = connection.createChannel();
			System.out.println("Channel is GOOOOOOOOOD");
			channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);
			channel.queuePurge(RPC_QUEUE_NAME);

			channel.basicQos(1);

			System.out.println(" [x] Awaiting RPC requests");
			System.out.println(0);
			Object monitor = new Object();
			DeliverCallback deliverCallback = (consumerTag, delivery) -> {
				AMQP.BasicProperties replyProps = new AMQP.BasicProperties
						.Builder()
						.correlationId(delivery.getProperties().getCorrelationId())
						.build();

				String response = "";
				System.out.println(1);

				try {
					String message = new String(delivery.getBody(), "UTF-8");
					System.out.println(2);
					boolean exists = am.bankAccountExistsFromCpr(message);  
					System.out.println(3);
					if (exists) {
						response = "true";
					}else{
						response = "false";
					}
					System.out.println(4);
				} catch (RuntimeException e) {
					System.out.println(" [.] " + e.toString());
				} finally {
					System.out.println(5);
					channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
					System.out.println(6);
					channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
					System.out.println(7);
					// RabbitMq consumer worker thread notifies the RPC server owner thread
					synchronized (monitor) {
						System.out.println(8);
						monitor.notify();
					}
				}
			};
			System.out.println(9);
			channel.basicConsume(RPC_QUEUE_NAME, false, deliverCallback, (consumerTag -> { }));
			// Wait and be prepared to consume the message from RPC client.
		} catch (Exception e){
			System.out.println("Something went wrong");
			e.printStackTrace();
		}
	}
}
