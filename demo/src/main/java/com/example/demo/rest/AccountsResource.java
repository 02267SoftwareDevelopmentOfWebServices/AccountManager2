package com.example.demo.rest;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import java.math.BigDecimal;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import org.json.JSONObject;

//@Path("/accounts/{user }/{initbalance}") - wrong
//@Path("/accounts/{accountId}") - When accessing a specific account
/**
 * 
 * @author Kevin
 *
 */
@Path("/accounts")
public class AccountsResource {

	public static AccountManager am = new AccountManager();

	public static RPCServerInterface rpcInterface = new RPCServer(am);

	/**
	 * This method creates a new bank account and registers a user in the in memory repository.
	 * @param input contains all the information needed to create a new user.
	 * @return a response, which is OK and contains a message, which indicates if the user was registered correctly
	 */
	@POST
	@Consumes ("text/plain")
	@Produces ("text/plain")
	public Response newAccountCustomer(String input) {
		System.out.println("AccountResource running now");
		JSONObject customer = new JSONObject(input);

		String cprNumber = customer.getString("cpr");
		System.out.println("AccountResource cpr: " + cprNumber);
		String firstName = customer.getString("firstName");
		System.out.println("AccountResource firstname: " + firstName);
		String lastName = customer.getString("lastName");
		System.out.println("AccountResource lastname: " + lastName);
		
		String type = customer.getString("type"); 
		System.out.println("AccountResource type: " + type);
		Customer customer_;
		Merchant merchant_;
		
		try {
			System.out.println("Client running...");

			BigDecimal bd = new BigDecimal(500.0);
			if (type.equals("customer")){
				System.out.println("bd: " + bd);
				customer_ = new Customer(firstName, lastName, new CprNumber(cprNumber));
				if (am.registerCustomerBank(customer_, bd)) {
				System.out.println("Customer is registered in bank");
			
				boolean result = am.registerCustomer(customer_);
//				System.out.println("repo customer size" + am.getRepo().customers.size());
//				System.out.println("repo merchant size" + am.getRepo().merchants.size());
				System.out.println("Customer registered in repo");
				return Response.ok(""+result).build();
				
				} else {
					return Response.ok("Customer already has a bank").build();
				}
			}else if (type.equals("merchant")){
				merchant_ = new Merchant(firstName, lastName, new CprNumber(cprNumber));
				if (am.registerMerchantBank(merchant_, bd)) {
					System.out.println("Merchant is registered in bank");
					boolean result = am.registerMerchant(merchant_);
//					System.out.println("repo customer size" + am.getRepo().customers.size());
//					System.out.println("repo merchant size" + am.getRepo().merchants.size());
					System.out.println("Merchant registered in repo");
					return Response.ok(""+result).build();
					
					} else {
						return Response.ok("Customer already has a bank").build();
					}
			}
			else{
				return Response.ok(null).build(); // TODO throw exception
			}
			
		} catch (Exception e) {
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}

	}

}