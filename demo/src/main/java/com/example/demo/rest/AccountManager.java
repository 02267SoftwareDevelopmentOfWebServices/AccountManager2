package com.example.demo.rest;
import java.math.BigDecimal;

/**
 * 
 * @author Kevin
 *
 */

public class AccountManager {
	
	private static InMemoryRepository repo = new InMemoryRepository();
	private BankServiceConnect bankService;	
	//static BankServiceConnect bankService = new BankServiceConnect();
	
	//RPCServerInterface RPCSI;

	public AccountManager () {
		//this.RPCSI = RPCSI;
		//this.repo = new InMemoryRepository();
		this.bankService = new BankServiceConnect();
		
	}
	
	// Customer ----------------------------------------------------------------------
	/**
	 * This method registers a customer in the bank by creating a new bank account if the customer does not already have one.  
	 * @param customer is the customer object to be registered
	 * @param initBalance is the initial amount of money on the new bank account
	 * @return a boolean value for successful registration or not
	 */
	public boolean registerCustomerBank(Customer customer, BigDecimal initBalance) {
		System.out.println("Init balance: " + initBalance);
		if (customer==null){
		}
		String accountID = this.bankService.createAccountWithBalance(customer, initBalance);
		System.out.println("Create account success");
		if(accountID !=null){
			customer.setAccountId(accountID);
			//TODO -> Also set if a merchant has the same CPR/account? 
			return true;
		}
		return false;
	}
	/**
	 * This method registers the customer in the local repository
	 * @param customer is the customer object to be registered
	 */
	public boolean registerCustomer(Customer customer) {
		// Check if Customer exists
		// Register to Repo
		if (!customerExists(customer.getCpr())) {
			repo.addCustomer(customer);
			return true;
		}
		return false;
	}
	/**
	 * This method checks if a customer with the given cpr number exists in the repository
	 * @param cprNumber is the cpr-number to be checked for 
	 * @return a boolean indicating if the customer exists or not
	 */
	public boolean customerExists(CprNumber cprNumber) {
		return repo.customerAlreadyExists(cprNumber);
	}
	
	
	// Merchant ------------------------------------------------------------------------
	/**
	 * This method registers a merchant in the bank by creating a new bank account if the merchant does not already have one.  
	 * @param merchant is the merchant object to be registered
	 * @param initBalance is the initial amount of money on the new bank account
	 * @return a boolean value for successful registration or not
	 */
	public boolean registerMerchantBank(Merchant merchant, BigDecimal initBalance) {
				
			String accountID = this.bankService.createAccountWithBalance(merchant, initBalance);
			if(accountID !=null){
				merchant.setAccountId(accountID);
				//TODO -> Also set if a customer has the same CPR/account? 
				return true;
			}
			return false;
	}
	/**
	 * This method registers the merchant in the local repository
	 * @param merchant is the merchant object to be registered
	 */
	public boolean registerMerchant(Merchant merchant) {
		System.out.println("I EXIST");
		if (!merchantExists(merchant.getCpr())) {
			repo.addMerchant(merchant);
			return true;
		} 
		return false;
	}
	/**
	 * This method checks if a merchant with the given cpr number exists in the repository
	 * @param cprNumber is the cpr-number to be checked for 
	 * @return a boolean indicating if the merchant exists or not
	 */

	public boolean merchantExists(CprNumber cprNumber) {
		return repo.merchantAlreadyExists(cprNumber);
	}


	/**
	 * this methods gets the repository 	
	 * @return the repository
	 */
	public InMemoryRepository getRepo() {
		return repo;
	}
	/**
	 * This method gets the account ID from a cpr number 
	 * @param cprNumber is the given cpr number 
	 * @return a String which is the accountID if the account exists or null if it does not exist. 
	 */
	public String getAccountIdFromBank(CprNumber cprNumber) {
		return bankService.getAccountFromCpr(cprNumber.get()).getId();
	}
	
	/**
	 * This method checks if a bank account with the given cpr number exists or not 
	 * @param cprNumber is the given cpr number
	 * @return a boolean value indicating if the bank account exists or not 
	 */
	public boolean bankAccountExistsFromCpr(CprNumber cprNumber) {
		if(bankService.getAccountFromCpr(cprNumber.get()) != null){
			return true; 
		}
		return false;
	}
	/**
	 * This method uses an accoundId to delete a merchant from the bank and the repository.
	 * @param accountId if the given accountID 
	 */
	public boolean deleteMerchantAccount(String accountId) {
		boolean bool = this.bankService.deleteAccount(accountId);
		repo.deleteMerchantFromAccountId(accountId);
		return bool;
	}
	/**
	 * This method uses and accountID to delete a customer fro mthe bank and the repository
	 * @param accountId is the given accountID 
	 */
	
	public boolean deleteCustomerAccount(String accountId) {
		boolean bool = this.bankService.deleteAccount(accountId);
		repo.deleteCustomerFromAccountId(accountId);
		return bool;
	}
	
	
	// ---------------------------------
	/**
	 * This method checks if a bank account with the given cpr number exists or not 
	 * @param cprNumber is the given cpr number as a string 
	 * @return a boolean value indicating if the bank account exists or not 
	 */
	public boolean bankAccountExistsFromCpr(String cprNumber) {
		if(this.bankService.getAccountFromCpr(cprNumber) != null){
			return true; 
		}
		return false;
	}


	/**
	 * This method changes the first name of a user from their cpr number 
	 * @param cpr is the given cpr number
	 * @param newFirstName is the new first name of the user 
	 * @return the user with changed first name  
	 */
	
	public User changeFirstName(CprNumber cpr, String newFirstName){
		System.out.println("change first name in accountManager");
		return repo.changeFirstName(cpr, newFirstName);
	}
	/**
	 * This method changes the last name of a user from their cpr number
	 * @param cpr is the given cpr number
	 * @param newLastName is the new last name of the user 
	 * @return the user with changed last name
	 */
	public User changeLastName(CprNumber cpr, String newLastName){
		System.out.println();
		return repo.changeLastName(cpr, newLastName);
	}
	
//	public static void changeLastName(User user, String newLastName){
//		user.setLastName(newLastName);
//		//BankServiceConnect.updateAccount(user);
//	}
//	

}
