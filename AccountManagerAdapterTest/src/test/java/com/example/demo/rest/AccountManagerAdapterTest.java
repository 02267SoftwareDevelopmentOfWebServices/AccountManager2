package com.example.demo.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.client.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

/*
 * How to run:
 * a) mvn thorntail:run
 * b.1) mvn package
 * b.2) java -jar demo-thorntail.jar (in mvn)
 */
/**
 * 
 * @author Martin
 *
 */
public class AccountManagerAdapterTest {
	Client client = ClientBuilder.newClient();
	WebTarget r;

	String cpr = "123456-7890";
	String cpr2 = "012345-6789";


	@Test
	public void testNewAccountCustomer() {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts");

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("firstName", "Test");
		jsonObj.put("lastName", "Person");
		jsonObj.put("cpr", "101010-9999");
		jsonObj.put("type", "customer");

		String combinedString = jsonObj.toString();
		String result = r.request().post(Entity.entity(combinedString,"text/plain"),String.class);

		System.out.println("testNewAccountCustomer: " + result);
		assertNotNull(result);

		//System.out.println("requested tokens: " + result);
	}
	
	@Test
	public void testNewAccountMerchant() {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts");

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("firstName", "Mr.");
		jsonObj.put("lastName", "Glass");
		jsonObj.put("cpr", "170333-8888");
		jsonObj.put("type", "merchant");

		String combinedString = jsonObj.toString();
		String result = r.request().post(Entity.entity(combinedString,"text/plain"),String.class);

		System.out.println("testNewAccountCustomer: " + result);
		assertNotNull(result);

		//System.out.println("requested tokens: " + result);
	}

	@Test
	public void testChangeCustomerAccount() {

		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts");

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("firstName", "Test");
		jsonObj.put("lastName", "Person");
		jsonObj.put("cpr", "101010-9999");

		String combinedString = jsonObj.toString();
		String result = r.request().post(Entity.entity(combinedString,"text/plain"),String.class);

		System.out.println("testChangeCustomerAccount - Account created: " +result);
		assertNotNull(result);

		String cpr = "101010-9999";
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts/" +cpr);

		String newName = "Real";
		String result2 = r.request().put(Entity.entity(newName,"text/plain"),String.class);

		System.out.println("testChangeCustomerAccount - Name changed: " + result2);
		assertNotNull(result2);

		//System.out.println("requested tokens: " + result);
	}

	@Test
	public void testDeleteCustomerAccount() {
	// Register account -----------------------
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts");

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("firstName", "Test");
		jsonObj.put("lastName", "Person");
		jsonObj.put("cpr", "101010-9999");

		String combinedString = jsonObj.toString();
		String result = r.request().post(Entity.entity(combinedString,"text/plain"),String.class);

		System.out.println("testDeleteCustomer - Register: " + result);
		assertNotNull(result);

		// Delete account -----------------------
		String cpr = "101010-9999";
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts/" +cpr);

		String result2 = r.request().delete(String.class);

		System.out.println("testDeleteCustomer - Delete: " + result2);
		assertNotNull(result2);

		//System.out.println("requested tokens: " + result);
	}
}

