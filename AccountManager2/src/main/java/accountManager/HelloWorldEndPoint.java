package accountManager;


import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/*
 * Part of the example given in task 7.
 * Use for inspiration, delete later.
 */

@Path("/hello")
public class HelloWorldEndPoint {

	@GET
	@Produces("text/plain")
	public Response doGet() {
		
		return Response.ok("Hello from Thorntail!").build();
	}
}
