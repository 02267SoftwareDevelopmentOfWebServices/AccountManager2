package accountManager;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/*
 * Part of the example given in task 7.
 * Use for inspiration.
 */

@ApplicationPath("/")
public class RestApplication extends Application {

}
